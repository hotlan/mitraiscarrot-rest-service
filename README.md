# Mitrais Carrot

## Installation

### Clone repo

``` bash
# clone the repo
$ git clone https://hotlan@bitbucket.org/hotlan/mitraiscarrot-rest-service.git

# go into app's directory
$ cd mitraiscarrot-rest-service
```

### create mitrais_carrot database and change the database connection in 
``` bash
# copy `src/main/resources/application.properties.example`
# and rename into `src/main/resources/application.properties`
#now change with your db config connection
```

### Run Application
``` bash
# install app's dependencies
$ mvn spring-boot:run

# Open browser
http://localhost:8989/
```

### View Swagger API Doc
``` bash
# view the swagger json
http://localhost:8989/v2/api-docs
# view the swagger UI
http://localhost:8989/swagger-ui.html
```
