package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.Transactions;
import org.springframework.data.repository.CrudRepository;

/**
 *
 */
public interface TransactionsRepository extends CrudRepository<Transactions, Integer> {

}
