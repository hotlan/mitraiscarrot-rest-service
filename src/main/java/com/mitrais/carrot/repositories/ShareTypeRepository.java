package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.ShareType;
import org.springframework.data.repository.CrudRepository;

/**
 *
 */
public interface ShareTypeRepository extends CrudRepository<ShareType, Integer> {

}
