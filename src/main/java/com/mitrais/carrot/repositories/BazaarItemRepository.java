package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.BazaarItem;
import org.springframework.data.repository.CrudRepository;

/**
 *
 */
public interface BazaarItemRepository extends CrudRepository<BazaarItem, Integer> {

}
