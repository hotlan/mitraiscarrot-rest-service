package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.Rewards;
import org.springframework.data.repository.CrudRepository;

/**
 *
 */
public interface RewardsRepository extends CrudRepository<Rewards, Integer> {

}
