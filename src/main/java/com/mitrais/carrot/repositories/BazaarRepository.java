package com.mitrais.carrot.repositories;

import com.mitrais.carrot.models.Bazaar;
import org.springframework.data.repository.CrudRepository;

/**
 *
 */
public interface BazaarRepository extends CrudRepository<Bazaar, Integer> {

}
